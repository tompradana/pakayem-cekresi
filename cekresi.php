<?php
/*
Plugin Name: 		Pakayem - Cekresi
Description: 		Plugin cek resi dan ongkir
Author: 			Tommy Pradana
Version: 			1.0.0
*/
if ( !function_exists( 'add_action' ) ) {
	exit;
}

define( 'PCR_PLUGIN_VERSION', '1.0.0' );
define( 'PCR_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'PCR_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'PCR_ENV', 'staging' );

include( PCR_PLUGIN_DIR . 'includes/functions.php' );