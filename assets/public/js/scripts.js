(function($) {
    /**
     * [$btn_add description]
     * @type {[type]}
     */
    let $btn_add = $('[btn=tambah-resi]');
    let $btn_lacak = $('[btn=lacak-resi]');
    let limit = 4;
    let options = "";

    /**
     * [description]
     * @param  {[type]} e){		e.preventDefault();		let area          [description]
     * @return {[type]}                                [description]
     */
    $btn_add.on('click', function(e) {
        e.preventDefault();
        let $this = $(this);
        let area = $(this).data('kurir');
        let kurir = _pcr_resi['kurir_' + area];

        if (limit != 0) {
            limit--;

            $.each(kurir, function(index, el) {
            	options += "<option value=\""+el.code+"\">"+el.label+"</option>"; 
            });
            let field = '<div class="field">\
				<input type="text" name="cek_resi['+(5-limit)+'][noresi]" placeholder="0001234441200">\
				<select name="cek_resi['+(5-limit)+'][kurir]">'+options+'</select>\
			</div>';

            $(this).closest('.actions').prev().append(field);
           
            if (limit == 0) {
                $(this).remove();
            }
        }
    });

    /**
     * [description]
     * @param  {[type]} e){                 	e.preventDefault();    } [description]
     * @return {[type]}      [description]
     */
    $btn_lacak.on('click',function(e){
    	e.preventDefault();
    	let formData = $(this).parents('.pcr-shortcode').find('form').serializeArray();
        let kurir = $(this).data('kurir');

        formData.push({name:"action",value:_pcr_resi.action+"_"+kurir});

        console.log(formData);

    	$.ajax({
    		url: _pcr_resi.ajaxurl,
    		type: 'POST',
    		data: formData,
    	}).done(function(response) {
    		console.log(response);
    	});
    })
})(jQuery);