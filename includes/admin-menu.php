<?php
/**
 * [pcr_admin_menu description]
 * @return [type] [description]
 */
function pcr_admin_menu() {
	add_menu_page( 'Settings', 'PakAyem Cekresi', 'administrator', 'pakayem-cekresi', 'pcr_admin_settings_page', '', 91 );
	add_submenu_page( 'pakayem-cekresi', 'Shortcode', 'Shortcode', 'administrator', 'pakayem-cekresi-shortcode', 'pcr_admin_shortcode_page' );
}
add_action( 'admin_menu', 'pcr_admin_menu' );

/**
 * [pcr_admin_menu_page description]
 * @return [type] [description]
 */
function pcr_admin_settings_page() {
	include PCR_PLUGIN_DIR . 'includes/views/admin/settings.php';
}

function pcr_admin_shortcode_page() {
	include PCR_PLUGIN_DIR . 'includes/views/admin/shortcode.php';
}