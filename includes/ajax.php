<?php
/**
 * [pcr_get_api_provider description]
 * @param  [type] $index [description]
 * @return [type]        [description]
 */
function pcr_get_api_provider( $index ) {
	$provider = get_option( 'pcr_local_courier_api' );
	return isset( $provider[$index] ) ? $provider[$index] : false;
};

/**
 * [pcr_request_response_handler description]
 * @param  [type]  $response     [description]
 * @param  string  $resi         [description]
 * @param  array   $kurir        [description]
 * @param  boolean $api_provider [description]
 * @return [type]                [description]
 */
function pcr_request_response_handler( $response = null, $resi = "", $kurir = [], $api_provider = false ) {
	return $response;
}

/**
 * [pcr_submit_cekresi description]
 * @param  [type] $resi         [description]
 * @param  array  $kurir        [description]
 * @param  [type] $api_provider [description]
 * @return [type]               [description]
 */
function pcr_submit_cekresi( $resi, $kurir = array(), $api_provider ) {
	/**
	 * RAJAONKGIR =============================
	 * [$api_provider description] 
	 * @var [type]
	 */
	if ( $api_provider == 1 ) {
		$response = wp_remote_post( 'https://pro.rajaongkir.com/api/waybill', array(
			'redirection' 	=> 10,
			'timeout'		=> 30,
			'httpversion'	=> '1.1',
			'headers'		=> array(
				'content-type' 	=> 'application/x-www-form-urlencoded',
				'key'			=> get_option( 'pcr_api_rajaongkir' )
			),
			'body'			=> array(
				'waybill'	=> $resi,
				'courier'	=> $kurir['code']
			)
		) );
		if ( !is_wp_error( $response ) ) {
			$response = wp_remote_retrieve_body( $response );
			return pcr_request_response_handler( $response, $resi, $kurir, $api_provider );
		} else {
			return false;
		}
	}

	/**
	 * BINDERBYTE =============================
	 * [$api_provider description]
	 * @var [type]
	 */
	else if ( $api_provider == 2 ) {
		return false;
	}

	/**
	 * AFTERSHIP =============================
	 */
	else {
		return false;
	}
}

/**
 * [pcr_cekresi_local description]
 * @return [type] [description]
 */
function pcr_cekresi_local() {
	$to_check 	= $_REQUEST['cek_resi'];
	$kurir 		= get_kurir( 'local' );
	$response 	= array();

	foreach( $to_check as $k => $v ) {
		$resi 			= $v['noresi'];
		$kurir_code 	= $v['kurir'];
		$index 			= array_search( $kurir_code, array_column( $kurir, 'code' ) );
		$api_provider 	= pcr_get_api_provider( $index );
		if ( false !== $api_provider && $resi ) {
			$response[] = pcr_submit_cekresi( $resi, $kurir[$index], $api_provider );
		}
	}
		
	wp_send_json( $response );

	exit;
}
add_action( 'wp_ajax_pcr_cekresi_local', 'pcr_cekresi_local' );
add_action( 'wp_ajax_noprive_pcr_cekresi_local', 'pcr_cekresi_local' );

/**
 * [prc_cekresi_single description]
 * @return [type] [description]
 */
function prc_cekresi_single() {
	wp_send_json( $_REQUEST );
	exit;
}
foreach( get_all_kurir() as $code => $name ) {
	add_action( "wp_ajax_pcr_cekresi_{$code}", 'prc_cekresi_single' );
	add_action( "wp_ajax_nopriv_pcr_cekresi_{$code}", 'prc_cekresi_single' );
}

