<?php
include PCR_PLUGIN_DIR . 'includes/admin-menu.php';
include PCR_PLUGIN_DIR . 'includes/kurir.php';
include PCR_PLUGIN_DIR . 'includes/shortcode.php';
include PCR_PLUGIN_DIR . 'includes/ajax.php';

/**
 * [pcr_register_plugin_scripts description]
 * @return [type] [description]
 */
function pcr_register_plugin_scripts() {
	$version = PCR_ENV === "staging" ? time() : PCR_PLUGIN_VERSION;
    wp_register_style( 'pakayem-cekresi', PCR_PLUGIN_URL . 'assets/public/css/style.css', array(), $version );
    wp_register_script( 'pakayem-cekresi', PCR_PLUGIN_URL . 'assets/public/js/scripts.js', array( 'jquery' ), $version, true );
}
add_action( 'wp_enqueue_scripts', 'pcr_register_plugin_scripts' );

/**
 * Logger :)
 */
if ( !function_exists( 'write_log' ) ) {
	function write_log( $log ) {
		if ( defined( 'PCR_ENV' ) && PCR_ENV === 'staging' ) {
			$logfile = PCR_PLUGIN_DIR . 'logs/debug.log';
			$log = is_array( $log ) || is_object( $log ) ? print_r( $log, true ) : $log;
			if ( defined( 'WP_DEBUG' ) && WP_DEBUG === true ) {
				error_log( $log, 3, $logfile );
			} else {
				file_put_contents( $logfile, $log.PHP_EOL , FILE_APPEND | LOCK_EX );
			}
		}
	}
}