<?php $local = get_kurir( 'local' ); ?>
<?php if ( isset( $_POST['provider'] ) ) {
	update_option( 'pcr_local_courier_api', $_POST['provider'] );
}; 
if ( isset( $_POST['api_rajaongkir'] ) ) {
	update_option( 'pcr_api_rajaongkir', trim( $_POST['api_rajaongkir'] ) );
}
if ( isset( $_POST['api_binderbyte'] ) ) {
	update_option( 'pcr_api_binderbyte', trim( $_POST['api_binderbyte'] ) );
}
if ( isset( $_POST['api_aftership'] ) ) {
	update_option( 'pcr_api_aftership', trim( $_POST['api_aftership'] ) );
}
$provider = get_option( 'pcr_local_courier_api' ); ?>
<div class="wrap">
	<h2>Pengaturan</h2>
	<div class="card">
		<form id="pcr-settings" method="POST">
			<p>
				<label>API RajaOngkir</label>
				<input class="widefat" type="text" name="api_rajaongkir" value="<?php echo get_option( 'pcr_api_rajaongkir' ); ?>">
			</p>
			<p>
				<label>API Binderbyte</label>
				<input class="widefat" type="text" name="api_binderbyte" value="<?php echo get_option( 'pcr_api_binderbyte' ); ?>">
			</p>
			<p>
				<label>API Aftership</label>
				<input class="widefat" type="text" name="api_aftership" value="<?php echo get_option( 'pcr_api_aftership' ); ?>">
			</p>
			<hr/>
			<table cellpadding="5" width="100%">
				<tbody>
				<?php foreach( $local as $i => $kurir ) : ?>
					<tr>
						<td><?php echo $kurir['label']; ?></td>
						<td>
							<select name="provider[]" class="widefat" style="width: 100%">
								<option value="1" <?php selected( $provider[$i], "1" ); ?>>Rajaongkir</option>
								<option value="2" <?php selected( $provider[$i], "2" ); ?>>Binderbyte</option>
								<option value="3" <?php selected( $provider[$i], "3" ); ?>>Aftership</option>
							</select>
						</td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
			<?php submit_button( 'Simpan', 'primary', 'submit', true, null ); ?>
		</form>
	</div>
</div>