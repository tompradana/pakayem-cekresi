<?php $kurir = get_all_kurir(); ?>
<script type="text/javascript">
	function get_shortcode(el) {
		document.getElementById('kurir-shortcode').value = '[pakayem_resi kurir="'+el.value+'"]';
	}
</script>
<div class="wrap">
	<h2>Shortcode Tersedia</h2>
	<div class="card">
		<table width="100%" cellpadding="4">
			<tbody>
				<tr>
					<th scope="row" align="left">Cek Resi Local</th>
					<td>
						<input readonly type="text" value='[pakayem_resi kurir="lokal"]' class="widefat" onclick="select()">
					</td>
				</tr>
				<tr>
					<th scope="row" align="left">Cek Resi China</th>
					<td>
						<input readonly type="text" value='[pakayem_resi kurir="china"]' class="widefat" onclick="select()">
					</td>
				</tr>
				<tr>
					<th scope="row" align="left">Cek Resi Global</th>
					<td>
						<input readonly type="text" value='[pakayem_resi kurir="global"]' class="widefat" onclick="select()">
					</td>
				</tr>
				<tr>
					<th scope="row" align="left">
						<select onchange="get_shortcode(this);">
							<?php foreach( $kurir as $code => $label ) : ?>
								<option value="<?php echo $code; ?>"><?php echo $label; ?></option>
							<?php endforeach; ?>
						</select>
					</th>
					<td>
						<input id="kurir-shortcode" readonly type="text" value='[pakayem_resi kurir="pos"]' class="widefat" onclick="select()">
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>