<?php $bulk 		= is_array( $list ) ? 'bulk-check' : "single-check"; ?>
<?php $placeholder 	= is_array( $kurir ) ? 'Masukkan nomor resi' : 'Masukkan nomor resi ' . $all_kurir[$kurir]; ?>
<?php $input_name 	= is_array( $kurir ) ? 'cek_resi[1][noresi]' : 'nomor_resi'; ?>
<div id="pcr-shortcode-resi-<?php echo $instance; ?>" class="pcr-shortcode">
	<form class="pcr-shortcode-resi <?php echo $bulk; ?>">
		<div class="field cf">
			<input type="text" name="<?php echo $input_name; ?>" placeholder="<?php echo $placeholder; ?>">
			<?php if ( is_array( $list ) ) : ?>
				<select name="cek_resi[1][kurir]">
					<?php foreach( $list as $i => $k ) : ?>
						<option value="<?php echo $k['code']; ?>"><?php echo $k['label']; ?></option>
					<?php endforeach; ?>
				</select>
			<?php else : ?>
				<a btn="lacak-resi" data-kurir="<?php echo $kurir; ?>" href="javascript://">Lacak</a>
			<?php endif; ?>
		</div>
	</form>
	<?php if ( is_array( $kurir ) ) : ?>
		<div class="actions cf">
			<a btn="tambah-resi" data-kurir="<?php echo $kurir; ?>" href="javascript://">+ Tambah resi</a>
			<a btn="lacak-resi" data-kurir="<?php echo $kurir; ?>" href="javascript://">Lacak</a>
		</div>
	<?php endif; ?>
</div>