<?php
function pcr_register_shortcode( $atts ) {
	extract( shortcode_atts( array(
		'kurir' => 'lokal'
	), $atts, 'pakayem_resi' ) );
	if ( $kurir == 'lokal' ) $kurir = "local";
	if ( in_array( $kurir, array( 'local', 'china', 'global' ) ) ) {
		$list = get_kurir( $kurir );
	} else {
		$all_kurir = get_all_kurir();
		$list = $kurir;
	}
	ob_start();

	static $instance = 1;
	if ( $instance == 1 ) {
		wp_enqueue_style( 'pakayem-cekresi' );
		wp_enqueue_script( 'pakayem-cekresi' );
	}

	wp_localize_script( 'pakayem-cekresi', '_pcr_resi', array(
		'ajaxurl'		=> admin_url( 'admin-ajax.php' ),
		'action'		=> 'pcr_cekresi',
		'kurir_local' 	=> get_kurir( 'local' ),
		'kurir_china' 	=> get_kurir( 'china' ),
		'kurir_global' 	=> get_kurir( 'global' )
	) );

	include PCR_PLUGIN_DIR . 'includes/views/public/shortcode-resi.php'; 
	$instance++;

	return ob_get_clean();
}
add_shortcode( 'pakayem_resi', 'pcr_register_shortcode' );