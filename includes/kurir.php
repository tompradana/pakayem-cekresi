<?php
/**
 * [get_kurir description]
 * @param  string $type [description]
 * @return [type]       [description]
 */
function get_kurir( $type = "all" ) {
	$kurir = array();

	// id untuk aftership,
	// code untuk rajaongkir dan binderbyte
	$kurir['local'] = array(
		array(
			'id' 	=> 0,
			'code' 	=> 'pos',
			'label' => 'POS'
		),
		array(
			'id' 	=> 0,
			'code' 	=> 'jne',
			'label' => 'JNE'
		),
		array(
			'id' 	=> 0,
			'code' 	=> 'jnt',
			'label' => 'J&T Express'
		),
		array(
			'id' 	=> 0,
			'code' 	=> 'tiki',
			'label' => 'TIKI'
		),
		array(
			'id' 	=> 0,
			'code' 	=> 'ninja',
			'label' => 'Ninja Express'
		),
		array(
			'id' 	=> 0,
			'code' 	=> 'sicepat',
			'label' => 'J&T Express'
		),
		array(
			'id' 	=> 0,
			'code' 	=> 'wahana',
			'label' => 'Wahana'
		),
		array(
			'id' 	=> 0,
			'code' 	=> 'anteraja',
			'label' => 'Anteraja'
		),
		array(
			'id' 	=> 0,
			'code' 	=> 'lion',
			'label' => 'Lion Parcel'
		),
		array(
			'id' 	=> 0,
			'code' 	=> 'lex',
			'label' => 'LEX'
		),
		array(
			'id' 	=> 0,
			'code' 	=> 'ide',
			'label' => 'ID Express'
		),
		array(
			'id' 	=> 0,
			'code' 	=> 'sap',
			'label' => 'SAP Express'
		),
		array(
			'id' 	=> 0,
			'code' 	=> '',
			'label' => 'Nusantara Card Semesta'
		),
		array(
			'id' 	=> 0,
			'code' 	=> 'ncs',
			'label' => 'Royal Express Indonesia'
		),
		array(
			'id' 	=> 0,
			'code' 	=> 'sc',
			'label' => 'Sentral Cargo'
		),
		array(
			'id' 	=> 0,
			'code' 	=> 'jet',
			'label' => 'JET Express'
		),
		array(
			'id' 	=> 0,
			'code' 	=> 'dse',
			'label' => '21 Express'
		),
		array(
			'id' 	=> 0,
			'code' 	=> '',
			'label' => 'First Logistic'
		),
		array(
			'id' 	=> 0,
			'code' 	=> 'first',
			'label' => 'IDL Cargo'
		),
		array(
			'id' 	=> 0,
			'code' 	=> '',
			'label' => 'J-Xxpress'
		),
		array(
			'id' 	=> 0,
			'code' 	=> 'rcl',
			'label' => 'Red Carpet Logistics'
		),
		array(
			'id' 	=> 0,
			'code' 	=> 'alfa',
			'label' => 'AlfaTrex'
		),
		array(
			'id' 	=> 0,
			'code' 	=> 'kurasi',
			'label' => 'Kurasi'
		),
		array(
			'id' 	=> 0,
			'code' 	=> 'tk',
			'label' => 'Trans Kargo'
		),
		array(
			'id' 	=> 0,
			'code' 	=> 'pandu',
			'label' => 'Pandu Logistik'
		),
		array(
			'id' 	=> 0,
			'code' 	=> 'mli',
			'label' => 'MGlobal Logistics Indonesia'
		),
	);

	// china aftership
	$kurir['china'] = array(
		array(
			'id' 	=> 0,
			'label' => 'China EMS'
		),
		array(
			'id' 	=> 0,
			'label' => 'China POS'
		),
		array(
			'id' 	=> 0,
			'label' => 'YUN Express'
		),
		array(
			'id' 	=> 0,
			'label' => 'Yanwen'
		),
		array(
			'id' 	=> 0,
			'label' => 'LWE'
		),
		array(
			'id' 	=> 0,
			'label' => 'Yamato Japan'
		),
		array(
			'id' 	=> 0,
			'label' => 'Acommerce'
		),
		array(
			'id' 	=> 0,
			'label' => 'Sunyou Post'
		)
	);

	// global aftership
	$kurir['global'] = array(
		array(
			'id' 	=> 0,
			'label' => 'Aramex'
		),
		array(
			'id' 	=> 0,
			'label' => 'Citylink'
		),
		array(
			'id' 	=> 0,
			'label' => 'Fedex'
		),
		array(
			'id' 	=> 0,
			'label' => 'Flyt Express'
		),
		array(
			'id' 	=> 0,
			'label' => 'TNT'
		),
		array(
			'id' 	=> 0,
			'label' => 'DHL Suplai Chain Indonesia'
		),
		array(
			'id' 	=> 0,
			'label' => 'UPS'
		),
		array(
			'id' 	=> 0,
			'label' => 'USPS'
		),
		array(
			'id' 	=> 0,
			'label' => 'Bluecare Express'
		),
		array(
			'id' 	=> 0,
			'label' => 'Pos Indonesia International'
		),
		array(
			'id' 	=> 0,
			'label' => 'Pos Laju'
		),
		array(
			'id' 	=> 0,
			'label' => 'Skynet Worldwide Express'
		),
		array(
			'id' 	=> 0,
			'label' => 'Janio Asia'
		),
		array(
			'id' 	=> 0,
			'label' => 'Quantium'
		),
		array(
			'id' 	=> 0,
			'label' => 'City-Link Express'
		),
		array(
			'id' 	=> 0,
			'label' => 'S.F Express'
		)
	);

	if ( $type == 'all' ) {
		return $kurir;
	} else {
		return $kurir[$type];
	}
}

/**
 * [get_all_kurir description]
 * @return [type] [description]
 */
function get_all_kurir( $kode = "" ) {
	$all = array_merge( wp_list_pluck( get_kurir( 'local' ), 'label', 'code' ), wp_list_pluck( get_kurir( 'china' ), 'label', 'code' ), wp_list_pluck( get_kurir( 'global' ), 'label', 'code' ) );
	return $all;
}